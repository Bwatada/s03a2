import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class WeekDay {

	// Store the input date, maximum 10
	public static final int MAXlENGTH = 10;
	ArrayList<LocalDate> myDate = new ArrayList<LocalDate>(MAXlENGTH);

	public void getInput(int count) {
		InputStream is = null;
		BufferedReader br = null;
		int flag = 0;

		try {
			is = System.in;
			br = new BufferedReader(new InputStreamReader(is));

			String line = null;

			System.out.println(
					"Please input the date in the format of 'MM,DD,YYYY', separate each date in a line and end input with double enter: ");
			while ((line = br.readLine()) != null && flag < count) {
				if (line.equalsIgnoreCase("quit")) {
					break;
				}
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM,dd,yyyy", Locale.ENGLISH);
				LocalDate date = LocalDate.parse(line, formatter);
				myDate.add(date);
				flag++;
			}
		} catch (IOException ioe) {
			System.out.println("Exception while reading input " + ioe);
		} finally {
			// close the streams using close method
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException ioe) {
				System.out.println("Error while closing stream: " + ioe);
			}
		}
	}

	private int leapYear(int year) {
		if ((year % 400 == 0) || (year % 100 == 0 && year % 4 == 0)) {
			return 1;
		} else
			return 0;
	}

	public String calculateDay(int year, int month, int day) {

		int result = 0;
		for (int i = 1; i <= year - 1; i++) {
			result += (leapYear(i) + 365) % 7;
		}
		for (int i = 1; i <= month - 1; i++) {
			if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12)
				result += 31;
			else if (i == 4 || i == 6 || i == 9 || i == 11)
				result += 30;
			else if (i == 2)
				result += (28 + leapYear(year));
		}
		result += day;
		result %= 7;

		String weekResult = null;
		switch (result) {
		case 1:
			weekResult = "Monday";
			break;
		case 2:
			weekResult = "Tuesday";
			break;
		case 3:
			weekResult = "Wednesday";
			break;
		case 4:
			weekResult = "Thursday";
			break;
		case 5:
			weekResult = "Friday";
			break;
		case 6:
			weekResult = "Saturday";
			break;
		case 0:
			weekResult = "Sunday";
			break;
		}
		return weekResult;
	}

	public void run() {
		int a, b, c;
		String myOutput;
		for (int i = 0; i < myDate.size(); i++) {
			a = myDate.get(i).getYear();
			b = myDate.get(i).getMonthValue();
			c = myDate.get(i).getDayOfMonth();
			myOutput = this.calculateDay(a, b, c);
			switch (i) {
			case 0:
				System.out.println("The " + (i + 1) + "st date you input is a " + myOutput);
				break;
			case 1:
				System.out.println("The " + (i + 1) + "nd date you input is a " + myOutput);
				break;
			case 2:
				System.out.println("The " + (i + 1) + "rd date you input is a " + myOutput);
				break;
			default:
				System.out.println("The " + (i + 1) + "th date you input is a " + myOutput);
				break;
			}
		}
		if (!myDate.isEmpty()) {
			myDate.clear();
		}
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		WeekDay weekday = new WeekDay();
		System.out.println("Please input the number of dates: ");
		Scanner in = new Scanner(System.in);
		int num = in.nextInt();
		if (num >= WeekDay.MAXlENGTH) {
			System.out.println("The length you input is larger than the limit!");
		} else if (num <= 0) {
			System.out.println("Illeagal input!");
		} else {
			weekday.getInput(num);
			weekday.run();
		}
		in.close();
	}

}
